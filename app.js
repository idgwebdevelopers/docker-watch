[
    ['warn',  '\x1b[33m'],
    ['error', '\x1b[31m'],
    ['info',   '\x1b[35m'],
    ['log',   '\x1b[2m']
].forEach((pair) => {
    let method = pair[0], reset = '\x1b[0m', color = '\x1b[36m' + pair[1];
    console[method] = console[method].bind(console, color, method.toUpperCase(), reset);
});

process.on('uncaughtException', (err) => {
    console.error(new Date(), 'Uncaught Exception:', err.stack ? err.stack : err);
    process.exit(1);
});

process.on('unhandledRejection', (err) => {
    console.error(new Date(), 'Unhandled Rejection:', err.stack ? err.stack : err);
    process.exit(1);
});


////////////////// CONFIGS //////////////////
const code = '/var/www/code';
const baseTarget = '/var/www/html/src';
const writeMode = 0o777 & ~process.umask();
process.umask(0);
/////////////////////////////////////////////

const fs = require('fs');
const path = require('path');
const execSync = require('child_process').execSync;
const chokidar = require('chokidar');
const rimraf = require('rimraf');

function executeCommand(command) {
    console.log('Command:', command);
    execSync(command);
}

function copyFileSync(source, target) {
    let targetFile = target;

    try {
        //if target is a directory a new file with the same name will be created
        if (fs.existsSync(target)) {
            if (fs.lstatSync(target).isDirectory()) {
                targetFile = path.join(target, path.basename(source));
            }
        }

        fs.writeFileSync(targetFile, fs.readFileSync(source), {
            mode: writeMode
        });
    } catch (e) {
        console.error('Failed to copy file:', source, target);
        throw e;
    }
}

function copyFolderRecursiveSync(source, target) {
    let files = [];

    try {
        //check if folder needs to be created or integrated
        let targetFolder = path.join(target, path.basename(source));
        if (!fs.existsSync(targetFolder)) {
            fs.mkdirSync(targetFolder, writeMode);
        }

        //copy
        if (fs.lstatSync(source).isDirectory()) {
            files = fs.readdirSync(source);
            files.forEach((file) => {
                let curSource = path.join(source, file);
                if (fs.lstatSync(curSource).isDirectory()) {
                    copyFolderRecursiveSync(curSource, targetFolder);
                } else {
                    copyFileSync(curSource, targetFolder);
                }
            });
        }
    } catch (e) {
        console.error('Failed to copy folder:', source, target);
        throw e;
    }
}

if (!process.env.DOCKER_WATCH_NO_COPY) {
    console.log('Copying current files...');
    executeCommand('cp -r ' + code + '/* ' + baseTarget + '/');
    executeCommand('chmod -R 777 ' + baseTarget + '/*');
}

chokidar.watch(code, {
    ignored: /(^|[\/\\])\../,
    usePolling: true,
    interval: 5000,
    awaitWriteFinish: {
        pollInterval: 500,
        stabilityThreshold: 2000
    },
    ignoreInitial: true,
    persistent: true,
    alwaysStat: true,
    awaitWriteFinish: true
}).on('all', (event, path) => {
    let newPath = path.replace(code, baseTarget);
    console.log(event, path);
    if (event === 'change' || event === 'add') {
        copyFileSync(path, newPath);
    } else if (event === 'unlink' || event === 'unlinkDir') {
        try {
            rimraf(newPath, () => {});
        } catch (e) {
            console.error('Failed to delete file:', newPath);
        }
    }
});
console.log('Watching files in ' + code + '...');
